<!DOCTYPE html>
  <head>
     <style>
    .google-maps {
        position: relative;
        padding-bottom: 75%; // This is the aspect ratio
        height: 0;
        overflow: hidden;
    }
    .google-maps > #map {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
</style>
  </head>
  <body>
  <div class="google-maps container">
    <div id="map"></div>
	</div>
    <script>
      var map;
	
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: new google.maps.LatLng(2.8,-187.3)
        });
		
        var script = map.data.loadGeoJson('https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson');;
       
	   var infowindow = new google.maps.InfoWindow();
		map.data.addListener('click', function(event) {
			var place = event.feature.getProperty("place");
			var datetime = event.feature.getProperty("time");
			var formattedTime = timeConverter(datetime);
			infowindow.setContent("<div style='width:150px;'>"+place+"</div><br/><div>"+formattedTime+"</div>");
			// position the infowindow on the marker
			infowindow.setPosition(event.feature.getGeometry().get());
			// anchor the infowindow on the marker
			infowindow.setOptions({pixelOffset: new google.maps.Size(0,-30)});
			infowindow.open(map);
		});
       
      }
      

      window.eqfeed_callback = function(results) {
        for (var i = 0; i < results.features.length; i++) {
          var coords = results.features[i].geometry.coordinates;
          var latLng = new google.maps.LatLng(coords[1],coords[0]);
          var marker = new google.maps.Marker({
            position: latLng,
            map: map
          });
        }
      }
	  function timeConverter(UNIX_timestamp){
		var a = new Date(UNIX_timestamp * 1000);
		var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
		var year = a.getFullYear();
		var month = months[a.getMonth()];
		var date = a.getDate();
		var hour = a.getHours();
		var min = a.getMinutes();
		var sec = a.getSeconds();
		var time = date + ' ' + month  + ' ' + hour + ':' + min + ':' + sec ;
		return time;
	  }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDhfnmKs2P_qpvTUbrBP8Psjr3o3CG8s-w&callback=initMap"></script>
  </body>
</html>