<?php

// Singleton to connect db.
class ConnectDb {
  // Hold the class instance.
  private static $instance = null;
  private $conn;
 
  private $host = 'localhost';
  private $user = 'db user-name';
  private $pass = 'db password';
  private $name = 'db name';
  
  // The db connection is established in the private constructor.
  private function __construct()
  {
    $this->conn = new mysqli($host, $user, $pass, $name);
  }
 
  public static function getInstance()
  {
    if(!self::$instance)
    {
      self::$instance = new ConnectDb();
    }
  
    return self::$instance;
  }
 
  public function getConnection()
  {
    return $this->conn;
  }
} 
?>