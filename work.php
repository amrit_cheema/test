<?php
require_once 'ConnectDb.php';

/**
*
* Submit form
*
* @param   null
* @return  boolean
*
*/
Function submitForm() {
     if(isset($_POST['submit'])) {
        if(validateForm($_POST)) {
            $postData = $_POST;
           
            //get db instance
            $instance = ConnectDb::getInstance();
            $conn = $instance->getConnection();
       
            // Check connection
            if ($conn->connect_error) {
                /* Note: we can log this error with error logs
                 die("Connection failed: " . $conn->connect_error);
                */
                return false;
            }
 
            $sql = "INSERT INTO Table (name, description, price) VALUES ($postData['name'], $postData['description'], $postData['price'])";
 
            if ($conn->query($sql) === TRUE) {
                return true;
            } else {
                /*Note: we can log this error with error logs
                echo "Error: " . $sql . "<br>" . $conn->error;
                */
                return false;
            }
 
            $conn->close();
        }
    }
}
 
/**
*
* Validate form
*
* @param   $object $data
* @return  boolean
*
*/
Function  validateForm($data) {
    $errors  = array();
    if (empty($data["name"])) {
        $errors[] = "Name is required";
    } else {
        $name = testField ($data["name"]);
        // check if name only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
            $errors[] = "Only letters and white space allowed in Name";
        }
    }
 
     if (empty($data["description"])) {
        $errors[] = "Description is required";
     } else {
        $description = testField($data["description"]);
        // check if description only contains letters and whitespace
        if (!preg_match("/^[a-zA-Z ]*$/",$description)) {
            $errors[] = "Only letters and white space allowed in Description";
        }
    }
 
    if (empty($data["price"])) {
        $errors[] = "Price is required";
     } else {
        $price = testField($data["price"]);
        if (is_numeric($price)) {
            $errors[] = "Only numbers allowed in price";
        }
    }
 
    if(count($errors) > 0) {
        return false;
    }     
        return true; 
    }
}
 
/**
*
* test field for security
*
* @param   string $field
* @return  string
*
*/
function testField($field) {
  $data = trim($field);
  $data = stripslashes($field);
  $data = htmlspecialchars($field);
  return $data;
}
?>
